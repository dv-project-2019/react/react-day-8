import React, { useState, useEffect } from 'react';
import { Descriptions } from 'antd';
import { List, Typography } from 'antd';
import { Select } from 'antd';
import { Button } from 'antd';

const TodoPage = (props) => {
    const { Option } = Select;

    const [user, setUser] = useState({});

    const [todoList, setTodoList] = useState([]);

    const [todoStatus, setTodoStatus] = useState([]);

    const [selectValue, setSelectValue] = useState('All');

    
    useEffect(() => {
        fetchUserDataById();
        fetchTodoData();
    }, [selectValue]);

    const fetchUserDataById = () => {
        //get user id
        const userId = props.match.params.user_id;

        fetch('https://jsonplaceholder.typicode.com/users/' + userId)
            .then(response => response.json())
            .then(data => {
                setUser(data);
            })
            .catch(error => console.log(error));
    }

    const fetchTodoData = () => {
        const userId = props.match.params.user_id;
        fetch('https://jsonplaceholder.typicode.com/todos?userId=' + userId)
            .then(response => response.json())
            .then(data => {
                setTodoList(data);
                checkTodoStatus(data);
            })
            .catch(error => console.log(error));
    }

    const checkTodoStatus = (todoList) => {
        var statusList = [];
        todoList.map((todo) => {
            if (!statusList.includes(todo.completed.toString())) {
                statusList.push(todo.completed.toString());
            }
        })
        setTodoStatus(statusList);
    }

    const selectTodoStatus = (value) => {
        setSelectValue(value);
    }

    const clickDone = (index) => {
        var todoUpdate = [...todoList];
        todoUpdate[index].completed = true;
        setTodoList(todoUpdate);
    }
 
    return (
        <div>
            <Descriptions title={"User : " + user.name}>
                <Descriptions.Item label="UserName">{user.username}</Descriptions.Item>
                <Descriptions.Item label="Telephone">{user.phone}</Descriptions.Item>
                <Descriptions.Item label="Website">{user.website}</Descriptions.Item>
                <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
            </Descriptions>

            <Select value={selectValue} style={{ width: 120 }} onChange={selectTodoStatus}>
                <Option value="All">All</Option>
                {todoStatus.map((todo, index) => {
                    if (todo == "false") {
                        return (
                            <Option key={index} value={todo}>Doing</Option>
                        )
                    } else if (todo == "true") {
                        return (
                            <Option key={index} value={todo}>Done</Option>
                        )
                    }
                })
                }
            </Select>

            <List
                header={<div>Todo List</div>}
                footer={<div>Footer</div>}
                bordered
                dataSource={
                    selectValue == 'All' ?
                        todoList
                        :
                        todoList.filter((list) => list.completed.toString() == selectValue)
                }
                renderItem={(item, index) => (
                    <List.Item>
                        {item.completed == true ?
                            <s> Done </s>
                            :
                            <Typography.Text mark> Doing </Typography.Text>
                        }
                        {item.title}
                        <Button onClick={() => clickDone(index)} value={index}>Done</Button>
                    </List.Item>
                )}
            />
        </div>
    )
}

export default TodoPage;