import React, { useState, useEffect } from 'react';
import { Layout, } from 'antd';
import { List, Card } from 'antd';

const { Header, Content, Footer } = Layout;

const AlbumPage = (props) => {

    const [albums, setAlbums] = useState([]);

    const userId = props.match.params.user_id;

    useEffect(() => {
        fetchAlbums();
    }, []);

    const fetchAlbums = () => {
        //get user id
        fetch('https://jsonplaceholder.typicode.com/albums?userId=' + userId)
            .then(response => response.json())
            .then(data => {
                setAlbums(data);
            })
            .catch(error => console.log(error));
    }

    console.log(albums)
    return (
        <Layout className="layout">
            <Header>
                <div className="logo" />
            </Header>
            <Content style={{ padding: '0 50px' }}>
                <div style={{ margin: '16px 0' }}>
                    <List
                        grid={{
                            gutter: 16,
                        }}
                        dataSource={albums}
                        renderItem={(album, index) => (
                            <List.Item>
                                <Card title={"Album No." + (index+1)}>
                                   <a href={"/users/" + userId + "/albums/" + album.id}><b>Album name:</b> {album.title}</a>
                                </Card>
                            </List.Item>
                        )}
                    /></div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>Copy Right ©2020 Created by Waan Thipnirun</Footer>
        </Layout>
    )
}
export default AlbumPage;