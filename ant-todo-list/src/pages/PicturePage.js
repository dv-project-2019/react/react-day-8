import React, { useState, useEffect } from 'react';
import { Layout, } from 'antd';
import { Card } from 'antd';
import { List, Avatar, Icon } from 'antd';

const PicturePage = (props) => {
    const [pictures, setPictures] = useState([]);
    const { Header, Content, Footer } = Layout;

    const { Meta } = Card;

    useEffect(() => {
        fetchAlbums();
    }, []);

    const fetchAlbums = () => {
        //get album id
        const albumId = props.match.params.album_id;

        fetch('https://jsonplaceholder.typicode.com/photos?albumId=' + albumId)
            .then(response => response.json())
            .then(data => {
                setPictures(data);
            })
            .catch(error => console.log(error));
    }

    const IconText = ({ type, text }) => (
        <span>
            <Icon type={type} style={{ marginRight: 8 }} />
            {text}
        </span>
    );

    return (
        <Layout className="layout">
            <Header>
                <div className="logo" />
            </Header>
            <Content style={{ padding: '0 50px' }}>
                <div style={{ margin: '16px 0' }}>
                    <List
                        itemLayout="vertical"
                        size="large"
                        pagination={{
                            onChange: page => {
                                console.log(page);
                            },
                            pageSize: 3,
                        }}
                        dataSource={pictures}
                        renderItem={picture => (
                            <List.Item
                                key={picture.title}
                                actions={[
                                    <IconText type="star-o" text="156" key="list-vertical-star-o" />,
                                    <IconText type="like-o" text="156" key="list-vertical-like-o" />,
                                    <IconText type="message" text="2" key="list-vertical-message" />,
                                ]}
                                extra={
                                    <img
                                        width={272}
                                        alt="logo"
                                        src={picture.url}
                                    />
                                }
                            >
                                <List.Item.Meta
                                    title={<a href={picture.url}>{picture.title}</a>}
                                    description={picture.thumbnailUrl}
                                />
                            </List.Item>
                        )}
                    />
                </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>Copy Right ©2020 Created by Waan Thipnirun</Footer>
        </Layout>
    )
}
export default PicturePage;
