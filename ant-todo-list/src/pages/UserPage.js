import React, { useState, useEffect } from 'react';
import { Input } from 'antd';
import { List, Avatar, Button, Skeleton } from 'antd';
import { Layout, Menu, Breadcrumb } from 'antd';

const { Search } = Input;
const { Header, Content, Footer } = Layout;

const UserPage = () => {
    const [users, setUsers] = useState([]);
    const [searchValue, setSearchValue] = useState('');
    const [ userId, setUserId] = ('');

    useEffect(() => {
        fetchAllUser();
        //fetchUser();
        console.log('useEffect');
    }, [userId]);

    const fetchAllUser = () => {
        fetch('https://jsonplaceholder.typicode.com/users' )
            .then(response => response.json())
            .then(data => {
                setUsers(data);
            })
            .catch(error => console.log(error));
    }

    const fetchUser = () => {
        fetch('https://jsonplaceholder.typicode.com/users/' + userId)
            .then(response => response.json())
            .then(data => {
                setUsers(data);
            })
            .catch(error => console.log(error));
    }

    const searchUserByName = (event) => {
        setSearchValue(event.target.value);
    }

    const checkUserId = (users) => {
        //console.log(users.length)
        var user = '';
        for(var i = 0; i < users.length; i++){
            if(users[i].name == searchValue){
                user = (users[i].id);
            }
        }
        setUserId(user);
    }

    return (
        <Layout className="layout">
            <Header>
                <div className="logo" />
            </Header>
            <Content style={{ padding: '0 50px' }}>
                <div style={{ margin: '16px 0' }}>
                    <Search
                        placeholder="input user name"
                        onChange={searchUserByName}
                        //onSearch={searchUserByName}
                        style={{ width: 300, height: 40 }}
                    />
                </div>
                <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
                    <List
                        className="demo-loadmore-list"
                        itemLayout="horizontal"
                        dataSource={users}
                        renderItem={item => (
                            <List.Item
                                actions={[<a href={"/users/" + item.id + "/todo"} >Todo</a>, 
                                <a href={"/users/" + item.id + "/albums"}>Albums</a>]}
                            >
                                <Skeleton avatar title={false} loading={item.loading} active>
                                    <List.Item.Meta
                                        avatar={
                                            <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                                        }
                                        title={<a href="https://ant.design">{item.name}</a>}
                                        description={item.email}
                                    />
                                </Skeleton>
                            </List.Item>
                        )}
                    />
                </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>Copy Right ©2020 Created by Waan Thipnirun</Footer>
        </Layout>
    );
}

export default UserPage;