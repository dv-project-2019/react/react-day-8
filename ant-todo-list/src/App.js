import React from 'react';
import logo from './logo.svg';
import './App.css';
import Search from './pages/Search';
import TodoPage from './pages/TodoPage';
import UserPage from './pages/UserPage';
import { BrowserRouter, Route } from 'react-router-dom';
import AlbumPage from './pages/AlbumPage';
import PicturePage from './pages/PicturePage';

function App() {
  return (
    <BrowserRouter>
      <Route path="/users" component={UserPage} exact={true}/>
      {/* //<Route path="/users/:user_id" component={UserPage} /> */}
      <Route path="/users/:user_id/todo" component={TodoPage} />
      <Route path="/users/:user_id/albums" component={AlbumPage} exact={true} />
      <Route path="/users/:user_id/albums/:album_id" component={PicturePage} />
    </BrowserRouter>
  );
}

export default App;
